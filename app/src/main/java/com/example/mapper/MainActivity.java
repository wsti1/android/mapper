package com.example.mapper;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import static com.example.mapper.MapActivity.X_COORDINATE_EXTRA;
import static com.example.mapper.MapActivity.Y_COORDINATE_EXTRA;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setEventListeners();
    }

    private void setEventListeners(){
        findViewById(R.id.katowiceGallery)
                .setOnClickListener(v -> runMapActivity(50.259410f, 19.018096f));
        findViewById(R.id.collegeOfInformationTechnology)
                .setOnClickListener(v -> runMapActivity(50.263381f, 19.013589f));
        findViewById(R.id.saucer)
                .setOnClickListener(v -> runMapActivity(50.265952f, 19.025250f));
        findViewById(R.id.polishNationalRadioSymphonyOrchestra)
                .setOnClickListener(v -> runMapActivity(50.264035f, 19.028285f));
    }

    private void runMapActivity(Float xCoordinate, Float yCoordinate){
        Intent intent = new Intent(getApplicationContext(), MapActivity.class);
        intent.putExtra(X_COORDINATE_EXTRA, xCoordinate);
        intent.putExtra(Y_COORDINATE_EXTRA, yCoordinate);
        startActivity(intent);
    }
}
