package com.example.mapper;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import java.util.Objects;

public class MapActivity extends AppCompatActivity {

    public static final String X_COORDINATE_EXTRA = "x";
    public static final String Y_COORDINATE_EXTRA = "y";

    WebView webView;

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        Bundle bundle = Objects.requireNonNull(getIntent().getExtras());
        String googleMapUrl = getGoogleMapUrl(bundle.getFloat(X_COORDINATE_EXTRA), bundle.getFloat(Y_COORDINATE_EXTRA));

        webView = findViewById(R.id.webView);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new WebViewClient());
        webView.loadUrl(googleMapUrl);
    }

    private String getGoogleMapUrl(Float xCoordinate, Float yCoordinate) {
        return String.format("https://maps.google.com/maps?q=%s,%s&t=m&z=13", xCoordinate, yCoordinate);
    }
}
